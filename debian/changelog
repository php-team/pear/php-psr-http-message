php-psr-http-message (2.0-1) experimental; urgency=medium

  [ Stilch ]
  * Add return typehints
  * Changed the requirements for the PHP version and
    removed declare strict types

  [ Aleksandr Golouz ]
  * Added string as return types for __toString
  * Added void as return types

  [ David Prévot ]
  * Revert "Track version 1 for now (bookworm?)"

 -- David Prévot <taffit@debian.org>  Tue, 04 Apr 2023 20:01:43 +0200

php-psr-http-message (1.1-1) experimental; urgency=medium

  * Upload new minor to experimental during the freeze

  [ Gabi DJ ]
  * Added PSR-7 Interfaces brief explanations and Basic Usage Guide (#79)

  [ Stilch ]
  * Adds parameter typehints

  [ David Prévot ]
  * Track version 1 for now (bookworm?)
  * Update standards version to 4.6.2, no changes needed.
  * Ship upstream documentation

 -- David Prévot <taffit@debian.org>  Tue, 04 Apr 2023 19:33:20 +0200

php-psr-http-message (1.0.1-3) unstable; urgency=medium

  * Fix d/watch to match updated GitHub URL
  * Use DEP-14 branch name
  * Bump debhelper from old 12 to 13.
  * debian/upstream/metadata:
    + Set: Bug-Database, Bug-Submit, Repository, Repository-Browse
    + Remove obsolete fields Contact, Name
  * Set Rules-Requires-Root: no.
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Install /u/s/pkg-php-tools/autoloaders file
  * Mark package as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sat, 25 Jun 2022 13:30:43 +0200

php-psr-http-message (1.0.1-2) unstable; urgency=medium

  * Update Homepage URI
  * Use versioned copyright format URI.
  * Update standards version, no changes needed.
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Contact, Name.
  * Drop get-orig-source target
  * Move repository to salsa.d.o
  * Update Standards-Version to 4.4.0

 -- David Prévot <taffit@debian.org>  Fri, 23 Aug 2019 10:13:29 -1000

php-psr-http-message (1.0.1-1) unstable; urgency=medium

  [ Matthew Weier O'Phinney ]
  * Added CHANGELOG for 1.0.1

  [ David Prévot ]
  * Use upstream changelog
  * Update Standards-Version to 4.1.0

 -- David Prévot <taffit@debian.org>  Thu, 21 Sep 2017 10:03:57 -1000

php-psr-http-message (1.0-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7
  * Rebuild with latest pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Thu, 03 Mar 2016 15:13:07 -0400

php-psr-http-message (1.0-1) unstable; urgency=low

  * Initial release

 -- David Prévot <taffit@debian.org>  Thu, 28 May 2015 13:58:57 -0400
